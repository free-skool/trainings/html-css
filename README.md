# Formation CSS

## Utiliser

Ce support utilise [revealJS](https://revealjs.com/) et [reveal-md](https://github.com/webpro/reveal-md).

Cloner le dépôt, installer le paquet reveal-md avec npm, puis le lancer.

```
git clone 
npm install
npm start
```

Renommer le projet, au niveau du dossier ainsi que dans `package.json`.

Remplacer l'url du dépôt par votre nouveau dépôt

```
git remote set-url origin <url>
```

## Générer le pdf

```
npm run build:pdf
```
