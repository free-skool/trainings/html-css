---
title: CSS
author: Emmanuelle Helly
---

# CSS : habiller le web (partie 2)

---

# Plan

----

- Sélecteurs avancés
- Responsive : mediaqueries et navigateur
- Accessibilité
- Inclure une police
- Positionnement avancé : Flexbox, grid, colonnes
- Trucs & Astuces : marges négatives, 
- Bonnes pratiques (id vs class, fond et forme, mobile first)
- Framework CSS
- SASS

---

## Sélecteurs avancés

----

par attribut

```css
a[attribut~="valeur"]
```

par élément fils direct

```css
a > b
```

par élément frère

```css
a + b
```

----

par fils premier, dernier ou tous les x éléments

```css
a:first-child
a:last-child
a:nth-child(expression)
```

négation

```css
a:not(.class)
```

première lettre, première ligne

```css
::first-letter
::first-line
```

Nous les verrons en détail en CSS avancé.
