# Exercices

- projet de base : un fichier html à habiller
- forker le projet sur gitlab et le clone sur son ordinateur

## Fichier de base

- entête de page
    - logo
    - menu
- contenu
    - liste de plats
    - pagination
- barre latérale
- pied de page

Remarque : utilisation de balises sémantiques `<section>`, `<header>`, `<footer>`, `<main>` et `<article>`

## Progression

### Voir le code

Observer le code, et les styles automatiquement appliqués

1. ouvrir l'inspecteur de code
1. sélectionner le titre `h1`
1. paramètres → afficher les styles du navigateur

### Normaliser les CSS

1. définir le modèle de boîte
1. changer la police de base
1. appliquer le site de base aux balses sémantiques (pour les anciens navigateurs)

### Mettre du style

1. colorer les titres, mettre h1 en petites majuscules
1. ajouter une bannière à l'entête

Propriétés :

- couleurs, polices et text transform
- background

### Délimiter les zones de la page

Appliquer une bordure ou un fond sur les zones de la page

Propriétés :

- marges, padding, background

### Organiser la page

1. Mettre le logo à gauche du titre
1. Agencer le contenu : sidebar à gauche et contenu principal à droite

Propriétés :

- flex ? float ? inline ?

### Intégrer les menus et la pagination

1. Mettre les items de menu en ligne
1. Agencer les liens de pagination en ligne

- float ? flex ? inline ?
