# HTML-CSS Filage

## Séance 1 : HTML bases (1/2 journée)

- durée : 3h30

### Pré-requis

- Faire le tuto HTML/CSS sur mozilla
- Lire un article (à choisir parmi 4 articles à définir)
- la veille : lire un cheatsheet HTML

### Déroulé

- Présentations : 10min
- Intro / rappels HTML bases : 20min
- Atelier créer ma première page : 20min
- Medias : 20min
- Atelier insérer des media dans ma page : 20min
- Pause : 10min
- Structure d'une page web : 20min
- Atelier organiser ma page web : 30min
- Formulaires : 20min
- Atelier créer un formulaire de contact : 40min

### Post séance

- [deboggage de code HTML](https://developer.mozilla.org/fr/docs/Apprendre/HTML/Introduction_%C3%A0_HTML/Debugging_HTML#Apprentissage_actif_%C3%A9tude_avec_un_code_permissif)

### Resources

- [Atelier structure page web](https://developer.mozilla.org/fr/docs/Apprendre/HTML/Introduction_%C3%A0_HTML/Structuring_a_page_of_content)

## Séance 2 : CSS (1/2 journée)

- Codepen ?
- Utiliser les mêmes noms de classe

### Pré-requis

- Faire le tuto HTML/CSS sur mozilla
- Lire une page du W3C (à choisir au choix)
- la veille : lire un cheatsheet CSS

### Déroulé

- Intro / rappels CSS Bases, sélecteurs, propriétés, unités : 40min
- Mise en page, positionnement, disposition des éléments dans la page : 30min
- Atelier mise en page : 30min
- Pause : 10min
- Atelier menu et galerie : 40min
- Mise en forme, couleurs, bordures, fonds, polices : 20min
- Atelier mise en forme : bannière, arrondi, couleurs et bordures : 40min

### Post séance

- Jeu hiérarchie des sélecteurs [flukeout.github.io](http://flukeout.github.io/)
- jeu flexbox
- jeu grid

## Séance 3 : Framework CSS ? TP ? Réutilisation et revue de code ? CSS avancé ?

### CSS notions avancée

- Responsive : mediaqueries et navigateur
- Inclure une police
- Trucs & Astuces : marges négatives, 
- Bonnes pratiques (id vs class, fond et forme, mobile first)
- Framework CSS
- SASS
