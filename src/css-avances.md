---
title: CSS
author: Emmanuelle Helly
theme: solarized
---

# CSS avancées

## (partie 2)

## 7 février 2020

----

## Sommaire

- Sélecteurs avancés
- Responsive : mediaqueries et navigateur
- Positionnement avancé : Flexbox, grid, column
- Trucs & Astuces : marges négatives
- Inclure une police
- Accessibilité
- SASS
- Framework CSS

----

## Ressources

- Documentation CSS : https://developer.mozilla.org/fr/docs/Web/CSS
- Blog CSS Tricks : https://css-tricks.com/
- Can I use : https://www.caniuse.com/

---

# Sélecteurs avancés

----

* Par attribut

    ```css
    a[attribut~="valeur"]
    ```

* Par élément fils direct

    ```css
    a > b
    ```

* Par élément frère

    ```css
    a + b
    ```

----

* Par fils premier, dernier ou tous les x éléments

    ```css
    a:first-child
    a:last-child
    a:nth-child(expression)
    ```

* première lettre, première ligne

    ```css
    ::first-letter
    ::first-line
    ```

* négation

    ```css
    a:not(.class)
    ```
----


## Hiérarchie des styles

Vers le plus important

- Implémentation du navigateur
- \> Styles <link ...>
- \> Styles définis dans <styles> dans la page HTML
- \> style défini dans la balise

----

## Hiérarchie de sélecteurs

sélecteur de classe > Sélecteur de balise > Sélecteur d'id

----

## Exercice

Déroulez le tutoriel [flukeout.github.io](http://flukeout.github.io/)

----

# Unités

## Absolues

* `px` : pour les supports écran
* `pt` : pour les supports imprimés

## Relatives

* `em`, `%`
* `rem` (root em), relative à la taille attibuée au document (nouveau !)

## Bonne pratique

Définir la taille de référence pour le `<body>`, puis définir les autres tailles en `em` ou `rem`

----

# Mise en forme

## Propriétés

TODO CSS2

* `background-size`, `border-radius`, `opacity`,
* `box-shadow` : [exemples](http://codepen.io/ericbutler555/pen/ogJdMg) [sur codepen](http://codepen.io/thomasjwicker/pen/jzbHt)
* dégradés
* arrière-plan multiples
* transparence pour la couleur de fond

[css3generator.com](http://css3generator.com/)


----

# Mise en page en CSS3

## Flexbox

    !css
    .flex {
        display: flex;
    }

    !html
    <div class="flex">
        <div>texte 1</div>
        <div>texte 2</div>
    </div>

Supporté par les navigateurs modernes, mais l'implémentation est parfois différente. Fonctionnel mais à utiliser à bon escient.

[Toutes les directives flex](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

Exemples de code : [système de grille](http://codepen.io/uxc/pen/xwxZZg), [Justify-content](http://codepen.io/chrisnager/pen/wBoXLE)

Amusant : [Générateur de Mondrian](http://codepen.io/phantomesse/pen/KmxBI)

----

# Partie 3 : Responsive design

----

# Sommaire

* Introduction
* Media-queries

----

# Introduction

Quantité de modèles de smartphones, de version de Android, IOS ou Windows Phone, de navigateurs et leurs versions, surchargés par les opérateurs ...
Impossible de tout couvrir.

## Responsive vs site dédié

Si le site web est une application métier, préférable de développer une application native, ou une version full mobile.

## Organisation et contenu responsive

* Organisation responsive : disposition varie en fonction de la définition d'écran
* Contenu responsive : certains contenus peuvent être affichés ou non selon la définition d'écran

----

# Media-queries

    @media (min-width: 700px) and (orientation: landscape) {…}

* Orientation (`portrait` ou `landscape`) et Localisation
* Device api (`screen`, `print`, `tv`, )

Importance de définir des points d'arrêt pertinents en fonction des terminaux les plus utilisés.

----

## Approche mobile first

    #sidebar {
        padding: 15px;
    }
    @media (min-width: 700px) {
        #sidebar {
            width: 33%;
            float: left;
        }
    }
    @media (min-width: 1040px) {
        #sidebar {
            width: 25%;
            float: left;
        }
    }

.fx: smaller

----

----

# Partie 4 : Frameworks

----

# Sommaire

* Découverte framework avec ou sans UI
* Étude de cas avec Bootstrap3

----

# Frameworks

## Sans UI

* [SimpleGrid](http://getsimplegrid.com/)
* [KNACSS](http://knacss.com/) par Raphaël Goetter et AlsaCreations

## Avec UI et composants animés

* [Blueprint](https://blueprintcss.dev/)
* [Materialize CSS](https://materializecss.com/)
* [Bootstrap](https://getbootstrap.com/)
* [Foundation](http://foundation.zurb.com/)

----

# Bootstrap 4 ?

## Sommaire

* Mise en forme CSS
* La grille bootstrap
* Composants

[Des exemples](http://getbootstrap.com/getting-started/#examples)

----

##  Mise en forme CSS

[getbootstrap.com/css](http://getbootstrap.com/css/)

* Typography (alignement, blockquotes, lists)
* Tables
* Formulaires
* Boutons
* Images

----

##  La grille bootstrap

* Le principe
* colonnes imbriquées
* offset
* ordre des colonnes

----

##  Composants

* Glyphicons
* Menus déroulants
* Navigation
* Breadcrumb, pagination, label, badges
* Media (groupe d'image + texte)
* Vignettes

----

## TP Bootstrap : Intégrer un design simple

Reprendre le fichier `tp-mise-en-page.html`

1.  Utilisez les classe Bootstrap pour mettre en oeuvre la grille
2.  Utiliser le HTML de bootstrap pour les éléments media, rendre les
    vignettes arrondies
3.  Utiliser le HTML pour le formulaire, rendre le bouton “envoyer” vert
    et “reset” rouge.
4.  Créer un carousel

Voir [les exemples](http://getbootstrap.com/getting-started/#examples) pour l'inspiration.

[Exemples de parallaxe](http://www.alsacreations.com/tuto/lire/1417-zoom-sur-effet-parallaxe.html)

[Générateur d'images](http://lorempixel.com/)
